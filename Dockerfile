FROM python:3.12-slim

# ベースイメージの更新と必要なパッケージのインストール
RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get install -y zsh libzmq-java curl build-essential git openjdk-17-jdk \
                       libzmq3-dev autoconf automake libtool sudo neovim raptor2-utils && \
    apt-get clean

# ユーザーの追加と権限設定
RUN useradd -m -s /bin/zsh glycobook && \
    chown -R glycobook:users /home/glycobook

ENV HOSTNAME glycobook

# sudoersの設定
RUN echo 'Defaults visiblepw' >> /etc/sudoers && \
    echo 'glycobook ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Pythonパッケージのインストール
RUN pip install jupyter jupyterlab

USER glycobook
WORKDIR /home/glycobook

# rbenvとruby-buildのインストール
RUN git clone https://github.com/sstephenson/rbenv.git ~/.rbenv && \
    git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

ENV PATH $PATH:/home/glycobook/.local/bin:/home/glycobook/.rbenv/shims:/home/glycobook/.rbenv/bin

# JRubyのインストールとirubyの登録
RUN rbenv install jruby-9.4.8.0 && \
    rbenv global jruby-9.4.8.0 && \
    gem install mime-types -v '3.5.2' && gem install iruby -v '0.7.4' && iruby register --force

# 作業ディレクトリの準備
RUN mkdir workspace
WORKDIR /home/glycobook/workspace

# Zshの設定
RUN echo "exec /bin/zsh" | sudo tee -a /etc/profile

# 必要なファイルのコピー
COPY ./.zshrc /home/glycobook/
COPY ./theme.zsh /home/glycobook/
COPY config.yaml /home/glycobook/.config/code-server/

RUN mkdir -p /home/glycobook/.local/bin /home/glycobook/.config/code-server

# ファイルの所有権と実行権限の設定
RUN sudo chown glycobook:users /home/glycobook/.zshrc /home/glycobook/theme.zsh && \
    sudo chmod +x /home/glycobook/.local/bin

# 追加のGemのインストール
RUN gem install specific_install

# code-serverのインストール
RUN curl -fsSL https://code-server.dev/install.sh | sh && \
    bash -c "$(curl --fail --show-error --silent --location https://raw.githubusercontent.com/zdharma-continuum/zinit/HEAD/scripts/install.sh)" && \
    sudo dpkg -i ~/.cache/code-server/*.deb

# ディレクトリの作成と権限設定
RUN mkdir ~/.glycobook && chmod 777 ~/.glycobook

# bash_kernelのインストール
RUN pip install bash_kernel
RUN python -m bash_kernel.install

# コンテナ起動時のコマンド
CMD ["jupyter", "notebook", "--port=8888", "--ip=0.0.0.0", "--allow-root", "--NotebookApp.token=''"]
